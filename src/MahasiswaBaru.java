public class MahasiswaBaru extends Mahasiswa{
    String asalSekolah;

    public boolean ikutOspek(){
        if(semester == 1){
            System.out.println("Harus Mengikuti Ospek, Karena baru semester 1");
            return true;
        } else{
            System.out.println("Tidak mengikuti Ospek, karena melibihi Semester 1");
            return false;
        }
    }

    @Override
    public void infoMahasiswa() {
        System.out.println("Asal Sekolah :" + this.asalSekolah);
        super.infoMahasiswa();
    }
}
